# C-HackCreate
A perl script to generate a C-Hack character sheet.
The progam spits out a text file that can be used in-game.
Walks the user through creating a character.

# What is C-Hack?
C-Hack started off as a collection of homebrew rules for a less-than-well known
ttrpg and spiralled into its own project.

## How do I play?
I'll update this section when I feel comfortable enough to take the draft rules
beyond my group.

# Usage
`c-hack_create [flags] [output file]`

Eventually I will post executables, but for now you have to install the Perl
dependencies yourself.

## Flags
* -d       : Debug mode
* -p [n]   : Sets the build points. If not set, default will be used.
* -a       : Advanced mode. Advanced mode is less descriptive. NYI
* -o [name]: Prints the character to the desinated file. If not specified,
prints to [character_name].txt
* --help   : Prints this.
* --version: Prints version number.

## Dependencies
* [Switch::Plain](https://metacpan.org/pod/Switch::Plain)
* [Getopt:Std](https://metacpan.org/pod/Getopt::Std)

These must be obtained with CPAN before the script will run.

# Todo
* [ ] Add advanced mode
* [ ] Add 'show' to show the current state of the character.
* [x] Add descriptions
* [ ] Fix subtraction of uninit values
* [ ] Add unused focus warning
* [ ] Allow skills input as txt file (remember calculations in txt file!)
* [ ] Sort skill list (hash random sorts, which is not ideal)
* [ ] Generate character archtypes (gunslinger, politician, doctor, etc.)
